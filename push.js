#!/usr/bin/env node

const oneSignal = require('onesignal-node');
const commandLineArgs = require('command-line-args');
const colors = require('colors/safe');
const path = require('path');
colors.enable();

const args = commandLineArgs([
  { name: 'help', alias: 'h', type: Boolean },
  { name: 'appId', type: String },
  { name: 'apiKey', type: String },
  { name: 'heading', type: String },
  { name: 'content', type: String, multiple: true, defaultOption: true }
]);

async function sendNotification({ appId, apiKey, content, heading = "" }) {
    if (!(appId && (typeof appId === 'string'))) throw new TypeError("Parameter appId must be non-empty string");
    if (!(apiKey && (typeof apiKey === 'string'))) throw new TypeError("Parameter apiKey must be non-empty string");
    if (typeof heading !== 'string') throw new TypeError("Parameter heading must be string");
    if (!(content && (typeof content === 'string'))) throw new TypeError("Parameter content must be non-empty string");
    
    const client = new oneSignal.Client(appId, apiKey);
    const response = await client.createNotification({
        ...(heading ? { headings: { en: heading } } : {}),
        contents: { en: args.content.join(" ") },
        included_segments: [ "Subscribed Users" ],
    });
    console.info(colors.green(`OneSignal response: ${JSON.stringify(response, null, "  ")}`));
}

if (args.help) {
    process.stdout.end(`
${path.basename(__filename)} [options] message...

${colors.underline(colors.bold("Options:"))}
  --help, -h    Prints this help
  --appId       ${colors.bold("Required.")} Sets OneSignal AppId
  --apiKey      ${colors.bold("Required.")} Sets OneSignal ApiKey
  --heading     Sets Push heading

${colors.underline(colors.bold("Examples:"))}
${path.basename(__filename)} --appId "app-id" --apiKey "api-key" Hello world
${path.basename(__filename)} --appId "app-id" --apiKey "api-key" --heading "Hello world" I am the script for sending PUSH notifications.

Have fun!

`);
    process.exit(process.exitCode = 0);
}

sendNotification({
    appId: args.appId,
    apiKey: args.apiKey,
    heading: args.heading || "",
    content: args.content ? args.content.join(" ") : "",
}).catch(
    (err) => {
        console.error(colors.red(`Error: ${err.message || err}`));
    }
)