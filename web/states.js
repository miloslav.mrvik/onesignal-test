function initSetupForm() {
    var $this = $(this);
    var $form = $this.find("form");

    $form.find("input[type=submit]").prop("disabled", false);
    var $input = $form.find("input[name='app-id']").val(localStorage.getItem("appId") || "");

    var onSubmit = function(evt) {
        evt.preventDefault();

        $form.find("input[type=submit]").prop("disabled", true);

        try {
            var appId = $form.find("input[name='app-id']").val();
            localStorage.setItem("appId", appId);

            var safariWebId = $form.find("input[name='safari-web-id']").val();
            if (safariWebId) {
                localStorage.setItem("safariWebId", safariWebId);
            } else {
                localStorage.removeItem("safariWebId");
            }

            setState("#all-set");
        } catch (err) {
            error(err);
        } finally {
            $form.find("input[type=submit]").prop("disabled", false);
        }
    };

    $form.on("submit", onSubmit);

    $this.one("deinit", function() {
        $form.off("submit", onSubmit);
    });

    setTimeout(function(){
        $input.select().focus();
    })
}

function initAllSet() {
    var $this = $(this);

    var appId = localStorage.getItem("appId");
    if (!appId) return setState("#setup-form")
    
    var safariWebId = localStorage.getItem("safariWebId");

    var $contents = $this.find(".contents");
    $contents.empty();

    var oneSignalInitOpts = { appId: appId };
    if (safariWebId) {
        oneSignalInitOpts.safari_web_id = safariWebId;
        oneSignalInitOpts.notifyButton = {
            enable: true,
        };
    }


    window.launchOneSignal(
        logger($contents),
        oneSignalInitOpts
    );
}

$(function () {
    OneSignal.push(function() {
        setState("#all-set");
    });
});