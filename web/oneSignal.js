async function launchOneSignal(log, initOpts) {
    log("Starting OneSignal...");
    var OneSignal = loggingProxy(log, window.OneSignal, "OneSignal");
    if (OneSignal.initialized) {
        log("OneSignal already initialized - reloading page in 3 s");
        return setTimeout(function() { location.reload(); }, 3000);
    } else {
        await OneSignal.init(initOpts);
    }

    OneSignal.showSlidedownPrompt();
}
