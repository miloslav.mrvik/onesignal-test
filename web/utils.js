
function toast(contents, header = "Message", delay = 5000) {
    var $toast = $('<div role="alert" aria-live="assertive" aria-atomic="true" class="toast"><div class="toast-header"><strong class="mr-auto"></strong><button type="button" class="ml-2 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body"></div></div>');
    $toast.find('.toast-header > strong').text(header);
    $toast.find('.toast-body').append(contents);

    var $cont = $('.toast-container');
    if (!$cont.length) {
        $cont = $('<aside/>').addClass('toast-container').css({ position: "fixed", top: "1rem", right: "1rem", width: "300px", margin: 0, padding: 0 });
        $(document.body).append($cont);
    }

    $cont.append($toast);

    $toast.toast({ autohide: (delay > 0), delay: delay });
    $toast.toast('show');
    $toast.on('hidden.bs.toast', function () {
        $toast.toast('dispose');
        $toast.remove();
    });
}

function error(err) {
    var $body = $('<code/>').text(err.message || err).wrap('<pre class="m-0" style="white-space:pre-wrap"/>').parent();
    toast($body, 'Error');
}

window.onerror = function (evt, source, lineno, colno, err) {
    if (!err) {
        if (typeof source === "string") {
            source = source.replace(new URL(".", location.href).toString(), "./");
        }
        err = new Error("Uncaught error at \n  " + source + ", line: " + lineno + ", col: " + colno);
    }
    error(err);    
}

function setState(selector) {
    var $oldState = $(".state:not(.d-none)");
    $oldState.addClass("d-none");
    $oldState.trigger("deinit");

    var $newState = $(selector)
    $newState.removeClass("d-none");

    var initFnName = $newState.data("init")
    if (typeof window[initFnName] === "function") {
        $newState.each(window[initFnName]);
    }
}

function logger($container) {
    return function (text, flavor = "light") {
        var $rec = $('<div/>');

        var $time = $('<small/>').css('display', 'block').text((new Date()).toLocaleTimeString());
        $rec.append($time);

        var $message = $('<code />').css('display', 'block');

        $rec.append($message.wrap('<pre class="m-0" style="white-space:pre-wrap"/>').parent());

        var handle = {
            get flavor() { return flavor; },
            set flavor(val) { flavor = val; $rec.each(function() { this.className = 'alert alert-' + flavor; }) },
            get text() { return text; },
            set text(val) { text = val; $message.text(text); }
        }

        handle.flavor = flavor;
        handle.text = text;

        $container.append($rec);

        return handle;
    }
}

function toInspect(x, padding = "") {
    var type = typeof x;
        switch (type) {
            case "object": {
                if (x === null) return `{null} null`;
                if (Array.isArray(x)) {
                    if (x.length === 0) return `{array} []`;
                    return `{array} [\n  ${padding}${x.map(function(e) {
                        return toInspect(e, padding + "  ");
                    }).join(",\n  " + padding)}\n${padding}]`;
                }

                var keys = Object.keys(x);
                var className = x.constructor.name || ((x instanceof Error) ? "Error" : "Object");
                if (keys.length === 0) return `{object} ${className} {}`;
                return `{object} ${className} {\n  ${padding}${keys.map(function(k) {
                    if (typeof k === "symbol") return k.toString() + ": " + toInspect(x[k], padding + "  ");
                    return JSON.stringify(k) + ": " + toInspect(x[k], padding + "  ");
                }).join(",\n  " + padding)}\n${padding}}`;
            }

            case "bigint":
            case "boolean":
            case "number":
                return "{"+type+"} " + x;
            
            case "undefined":
                return "{"+type+"}";

            case "symbol":
                return "{"+type+"} " + x.toString();

            case "function":
                return "{"+type+"} " + x.toString().split("\n")[0].replace(/(\{|=>).*/, "{ ... }");
            
            case "string":
                return "{"+type+"} " + JSON.stringify(x);
        }
}

function loggingProxy (log, target, name = "something") {
    var origSym = Symbol("original");
    Object.defineProperty(target, origSym, {
        configurable: false,
        writable: false,
        enumerable: false,
        value: target
    });

    var mkProxy = function(target) {
        var prox;
        return prox = new Proxy(target, {
            get: function (target, prop, receiver) {
                var rec = log("GET: " + name + "[" + toInspect(prop) + "]");
                var result;
                try {
                    result = Reflect.get(target, prop, receiver);
                    rec.flavor = "success";
                    rec.text = rec.text + "\n@returned " + toInspect(result);
                } catch (err) {
                    rec.flavor = "danger";
                    rec.text = rec.text + "\n@throwed " + toInspect(err);
                    throw err;
                }

                if (typeof result === "function") {
                    var fn = result;
                    var fnName = 
                    result = function (...args) {
                        var argsInspect = toInspect(args).split("\n");
                        argsInspect[0] = "";
                        argsInspect[argsInspect.length - 1] = "";
                        var rec = log("CALL: " + name + "[" + toInspect(prop) + "](" + argsInspect.join("\n") + ")");
                        var callResult;
                        try {
                            callResult = fn.apply((this === prox) ? target : this, args);
                        } catch (err) {
                            rec.flavor = "danger";
                            rec.text = rec.text + "\n@throwed " + toInspect(err);
                            throw err;
                        }

                        if (callResult instanceof Promise) {
                            rec.text = rec.text + "\n@returned {Promise}";
                            callResult.then(
                                function (res) {
                                    rec.flavor = "success";
                                    rec.text = rec.text + "\n  @resolved " + toInspect(res), "  ";
                                },
                                function (err) {
                                    console.error(err);
                                    rec.flavor = "danger";
                                    rec.text = rec.text + "\n  @rejected " + toInspect(err, "  ");
                                }
                            )
                        } else {
                            rec.flavor = "success";
                            rec.text = rec.text + "\n@returned " + toInspect(callResult);
                        }

                        return callResult;
                    }
                }

                return result;
            },
            set: function (target, prop, receiver) {
                var rec = log("SET: " + name + "[" + toInspect(prop) + "] to " + toInspect(val));
                var result;
                try {
                    result = Reflect.get(target, prop, receiver);
                    rec.flavor = "success";
                    rec.text = rec.text + " => " + toInspect(result);
                } catch (err) {
                    rec.flavor = "danger";
                    rec.text = rec.text + " throwed " + toInspect(err);
                    throw err;
                }
            }
        });
    };

    return mkProxy(target);
}